<?php
/**
 * Created by PhpStorm.
 * User: mike
 * Date: 26.01.16
 * Time: 21:55
 */

class EventFields {

    public static function loadFields() {
        register_field_group(array(
            'id' => 'acf_event_fields',
            'title' => 'Extra fields',
            'fields' => array(
                array (
                    'key' => 'field_56b8f1ecb7820',
                    'label' => 'Event settings',
                    'name' => 'event_settings',
                    'type' => 'repeater',
                    'required' => 1,
                    'sub_fields' => array (
                        array (
                            'key' => 'field_56b8f249b7821',
                            'label' => 'Event date',
                            'name' => 'event_date',
                            'type' => 'relationship',
                            'required' => 1,
                            'column_width' => '60',
                            'return_format' => 'object',
                            'post_type' => array (
                                0 => 'tcode_event-day',
                            ),
                            'taxonomy' => array (
                                0 => 'all',
                            ),
                            'filters' => array (
                                0 => 'search',
                            ),
                            'result_elements' => array (
                                0 => 'post_title',
                            ),
                            'max' => 1,
                        ),
                        array (
                            'key' => 'field_56b8f295b7822',
                            'label' => 'Event time',
                            'name' => 'event_time',
                            'type' => 'timepicker',
                            'required' => 1,
                            'column_width' => '20',
                            'default_value' => '',
                            'placeholder' => '',
                            'prepend' => '',
                            'append' => '',
                            'formatting' => 'none',
                            'maxlength' => '',
                        ),
                        array (
                            'key' => 'field_56b9cc90aa23b',
                            'label' => 'Event location',
                            'name' => 'event_location',
                            'type' => 'taxonomy',
                            'taxonomy' => 'location',
                            'field_type' => 'select',
                            'column_width' => '20',
                            'allow_null' => 1,
                            'load_save_terms' => 0,
                            'return_format' => 'object',
                            'multiple' => 0,
                        ),
                    ),
                    'row_min' => '1',
                    'row_limit' => '',
                    'layout' => 'table',
                    'button_label' => 'Add Event date',
                ),
                array(
                    'key' => 'field_event_url',
                    'label' => 'Read more url',
                    'name' => 'event_url',
                    'type' => 'link_picker',
                ),
                array (
                    'key' => 'field_56ba1fc05902c',
                    'label' => 'Event author',
                    'name' => 'event_artist',
                    'type' => 'relationship',
                    'return_format' => 'object',
                    'post_type' => array (
                        0 => 'tcode_artist',
                    ),
                    'taxonomy' => array (
                        0 => 'all',
                    ),
                    'filters' => array (
                        0 => 'search',
                    ),
                    'result_elements' => array (
                        0 => 'featured_image',
                        1 => 'post_title',
                    ),
                    'max' => 1,
                ),
            ),
            'location' => array(
                array(
                    array(
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'tcode_event',
                        'order_no' => 0,
                        'group_no' => 0,
                    ),
                ),
            ),
            'options' => array(
                'position' => 'normal',
                'layout' => 'default',
                'hide_on_screen' => array(),
            ),
            'menu_order' => 0,
        ));
    }
}


<?php
/**
 * Created by PhpStorm.
 * User: Shadov
 * Date: 2016-02-03
 * Time: 11:00
 */

// Include admin scripts and styles
add_action('admin_enqueue_scripts', function() {
    wp_enqueue_style('bootstrap-forms', TCODE_ES_URL . '/assets/css/bootstrap-forms.min.css');
    wp_enqueue_style('bootstrap-colorpicker-style', TCODE_ES_URL . '/assets/plugins/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css');
    wp_enqueue_style('2code-es-admin-style', TCODE_ES_URL . '/assets/css/admin-style.css');
    wp_enqueue_script('bootstrap', TCODE_ES_URL . '/assets/js/bootstrap.min.js', array('jquery'));
    wp_enqueue_script('bootstrap-colorpicker', TCODE_ES_URL . '/assets/plugins/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js', array('bootstrap'));
    wp_enqueue_script('2code-es-admin-script', TCODE_ES_URL . '/assets/js/admin-script.js', array('bootstrap-colorpicker'));
});

// Register events settings page.
add_action('admin_menu', function () {
    add_options_page(
        'Events Settings',
        'Events',
        'manage_options',
        '2code-event-schedule',
        function () {
            // Render options form.
            include TCODE_ES_DIR . '/assets/views/settings-page.php';
        }
    );
});

// Register settings sections.
add_action('admin_init', function () {
    add_settings_section('2code-event-schedule-general', 'General Settings', null, '2code-event-schedule');
});

// Color setting
add_action('admin_init', function () {
    $pageName    = '2code-event-schedule';
    $sectionName = '2code-event-schedule-general';
    $settingName = '2code_es_general_color';

    register_setting($pageName, $settingName);

    add_settings_field($settingName, 'Main color', function () use ($settingName) {
        printf(
            '<div class="input-group cpicker">
                <span class="input-group-addon"><i></i></span>
                <input type="text" value="%2$s" name="%1$s" class="form-control" />
            </div>',
            $settingName,
            get_option($settingName, '#d95353')
        );
    }, $pageName, $sectionName);
});

// Date format setting
add_action('admin_init', function () {
    $pageName    = '2code-event-schedule';
    $sectionName = '2code-event-schedule-general';
    $settingName = '2code_es_general_date_format';

    register_setting($pageName, $settingName);

    add_settings_field($settingName, 'Date format', function () use ($settingName) {
        $value = get_option($settingName, 'D • d/m/Y');

        $options = '';
        $optionsArray = array(
            'd-M-Y' => '15-Jan-2016',
            'F d, Y' => 'January 15, 2016',
            'd/m/Y' => '15/01/2016',
            'D • d/m/Y' => 'FRI • 15/01/2016',
            'd-m-Y' => '15-01-2016',
            'd.m.Y' => '15.01.2016'
        );

        foreach ($optionsArray as $val => $option) {
            $selected = $val === $value ? ' selected="selected"' : '';
            $options .= sprintf('<option value="%1$s"%2$s>%3$s</option>', $val, $selected, $option);
        }

        printf(
            '<select class="form-control" name="%1$s">%2$s</select>',
            $settingName,
            $options
        );
    }, $pageName, $sectionName);
});

// Hide dates
add_action('admin_init', function () {
    $pageName    = '2code-event-schedule';
    $sectionName = '2code-event-schedule-general';
    $settingName = '2code_es_general_date_hidden';

    register_setting($pageName, $settingName);

    add_settings_field($settingName, 'Date visibility', function () use ($settingName) {
        $values = array(
            'false' => 'Show date',
            'true' => 'Hide date'
        );

        foreach ($values as $key => $value) {
            $selected = $key === get_option($settingName, 'false') ? ' checked="checked"' : '';

            printf(
                '<div><label><input type="radio" class="form-control" name="%1$s" value="%2$s"%3$s/> %4$s</label></div>',
                $settingName,
                $key,
                $selected,
                $value
            );
        };
    }, $pageName, $sectionName);
});

// Time format setting
add_action('admin_init', function () {
    $pageName    = '2code-event-schedule';
    $sectionName = '2code-event-schedule-general';
    $settingName = '2code_es_general_time_format';

    register_setting($pageName, $settingName);

    add_settings_field($settingName, 'Time format', function () use ($settingName) {
        $values = array(
            'H:i' => '24h',
            'h:i A' => '12h AM/PM'
        );

        foreach ($values as $key => $value) {
            $selected = $key === get_option($settingName, 'h:i A') ? ' checked="checked"' : '';

            printf(
                '<div><label><input type="radio" class="form-control" name="%1$s" value="%2$s"%3$s/> %4$s</label></div>',
                $settingName,
                $key,
                $selected,
                $value
            );
        };
    }, $pageName, $sectionName);
});

// Image format setting
add_action('admin_init', function () {
    $pageName    = '2code-event-schedule';
    $sectionName = '2code-event-schedule-general';
    $settingName = '2code_es_general_image_format';

    register_setting($pageName, $settingName);

    add_settings_field($settingName, 'How to display event image?', function () use ($settingName) {
        $values = array(
            'circle' => 'Show image in a circle (image will be cropped)',
            'rectangle' => 'Show image in a rectangle',
            'svg' => 'Show svg icon as event image',
            'none' => 'None (don\'t show event image)'
        );

        foreach ($values as $key => $value) {
            $selected = $key === get_option($settingName, 'circle') ? ' checked="checked"' : '';

            printf(
                '<div><label><input type="radio" class="form-control" name="%1$s" value="%2$s"%3$s/> %4$s</label></div>',
                $settingName,
                $key,
                $selected,
                $value
            );
        };
    }, $pageName, $sectionName);
});

// Locations mode setting
add_action('admin_init', function () {
    $pageName    = '2code-event-schedule';
    $sectionName = '2code-event-schedule-general';
    $settingName = '2code_es_general_locations_mode';

    register_setting($pageName, $settingName);

    add_settings_field($settingName, 'Locations mode', function () use ($settingName) {
        $values = array(
            'false' => 'Single location (location selector will be hidden)',
            'true' => 'Multiple locations (location selector will be visible)'
        );

        foreach ($values as $key => $value) {
            $selected = $key === get_option($settingName, 'true') ? ' checked="checked"' : '';

            printf(
                '<div><label><input type="radio" class="form-control" name="%1$s" value="%2$s"%3$s/> %4$s</label></div>',
                $settingName,
                $key,
                $selected,
                $value
            );
        };
    }, $pageName, $sectionName);
});

// Event separator setting
add_action('admin_init', function () {
    $pageName    = '2code-event-schedule';
    $sectionName = '2code-event-schedule-general';
    $settingName = '2code_es_general_event_separator';

    register_setting($pageName, $settingName);

    add_settings_field($settingName, 'Event separator', function () use ($settingName) {
        $value = get_option($settingName, 'none');

        $options = '';
        $optionsArray = array(
            '1px solid' => 'Single line',
            '1px dotted' => 'Dotted line',
            '1px dashed' => 'Dashed line',
            'none' => 'No line',
        );

        foreach ($optionsArray as $val => $option) {
            $selected = $val === $value ? ' selected="selected"' : '';
            $options .= sprintf('<option value="%1$s"%2$s>%3$s</option>', $val, $selected, $option);
        }

        printf(
            '<select class="form-control" name="%1$s">%2$s</select>',
            $settingName,
            $options
        );
    }, $pageName, $sectionName);
});

// Open first accordion
add_action('admin_init', function () {
    $pageName    = '2code-event-schedule';
    $sectionName = '2code-event-schedule-general';
    $settingName = '2code_es_general_accordion_open_first';

    register_setting($pageName, $settingName);

    add_settings_field($settingName, 'Open first event accordion', function () use ($settingName) {
        $values = array(
            'false' => 'All accordions collapsed',
            'true' => 'First accordion expanded'
        );

        foreach ($values as $key => $value) {
            $selected = $key === get_option($settingName, 'false') ? ' checked="checked"' : '';

            printf(
                '<div><label><input type="radio" class="form-control" name="%1$s" value="%2$s"%3$s/> %4$s</label></div>',
                $settingName,
                $key,
                $selected,
                $value
            );
        };
    }, $pageName, $sectionName);
});

// Accordion behavior setting
add_action('admin_init', function () {
    $pageName    = '2code-event-schedule';
    $sectionName = '2code-event-schedule-general';
    $settingName = '2code_es_general_accordion_behavior';

    register_setting($pageName, $settingName);

    add_settings_field($settingName, 'Accordion behavior', function () use ($settingName) {
        $values = array(
            'false' => 'Collapse accordion when another element is expanded',
            'true' => 'Keep accordions open'
        );

        foreach ($values as $key => $value) {
            $selected = $key === get_option($settingName, 'false') ? ' checked="checked"' : '';

            printf(
                '<div><label><input type="radio" class="form-control" name="%1$s" value="%2$s"%3$s/> %4$s</label></div>',
                $settingName,
                $key,
                $selected,
                $value
            );
        };
    }, $pageName, $sectionName);
});

// Link anchor setting
add_action('admin_init', function () {
    $pageName    = '2code-event-schedule';
    $sectionName = '2code-event-schedule-general';
    $settingName = '2code_es_general_link_anchor';

    register_setting($pageName, $settingName);

    add_settings_field($settingName, '"Read more" link anchor', function () use ($settingName) {
        printf(
            '<input class="form-control" name="%1$s" value="%2$s"/>',
            $settingName,
            get_option($settingName, 'Read more...')
        );
    }, $pageName, $sectionName);
});

// Link class setting
add_action('admin_init', function () {
    $pageName    = '2code-event-schedule';
    $sectionName = '2code-event-schedule-general';
    $settingName = '2code_es_general_link_class';

    register_setting($pageName, $settingName);

    add_settings_field($settingName, '"Read more" link class', function () use ($settingName) {
        printf(
            '<input class="form-control" name="%1$s" value="%2$s"/>',
            $settingName,
            get_option($settingName)
        );
        printf('<p class="text-muted">Using this option you can set "Read more" links to display like other buttons on your site. Simply copy&paste your button\'s class.<br>For standard bootstrap button type: <code>btn btn-primary</code><br>Leave empty to display the link as text.</br></p>');
    }, $pageName, $sectionName);
});

// ACF setting
add_action('admin_init', function () {
    $pageName    = '2code-event-schedule';
    $sectionName = '2code-event-schedule-general';
    $settingName = '2code_es_general_use_builtin_acf';

    register_setting($pageName, $settingName);

    add_settings_field($settingName, 'ACF settings', function () use ($settingName) {
        $values = array(
            'true' => 'Use ACF embedded into event-schedule plugin',
            'false' => 'Use third-party ACF installation (choose this option if your theme requires ACF usage)'
        );

        foreach ($values as $key => $value) {
            $selected = $key === get_option($settingName, 'true') ? ' checked="checked"' : '';

            printf(
                '<div><label><input type="radio" class="form-control" name="%1$s" value="%2$s"%3$s/> %4$s</label></div>',
                $settingName,
                $key,
                $selected,
                $value
            );
        };
    }, $pageName, $sectionName);
});
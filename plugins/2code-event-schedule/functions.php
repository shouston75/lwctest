<?php
/**
 * Created by PhpStorm.
 * User: mike
 * Date: 26.01.16
 * Time: 21:24
 */

// Include fields
include_once TCODE_ES_DIR . '/Fields/DayFields.php';
include_once TCODE_ES_DIR . '/Fields/ArtistFields.php';
include_once TCODE_ES_DIR . '/Fields/EventFields.php';

// Include Types
include_once TCODE_ES_DIR . '/Types/DayType.php';
include_once TCODE_ES_DIR . '/Types/ArtistType.php';
include_once TCODE_ES_DIR . '/Types/EventType.php';

// Include settings page
include_once TCODE_ES_DIR . '/Includes/settings-page.php';

// Setup frequently used image sizes 
add_image_size('2code-square-thumbnail', 90, 90, true);
add_image_size('2code-rect-thumbnail', 140, 90, array('center', 'center'));

add_action('after_setup_theme', function() {
    global $_wp_theme_features;

    if (!is_array($_wp_theme_features['post-thumbnails'])) {
        $_wp_theme_features['post-thumbnails'] = array();
        $_wp_theme_features['post-thumbnails'][0] = array(
            'tcode_event',
            'tcode_artist'
        );

        if (!in_array('page', $_wp_theme_features['post-thumbnails'][0])) {
            $_wp_theme_features['post-thumbnails'][0][] = 'page';
        }

        if (!in_array('post', $_wp_theme_features['post-thumbnails'][0])) {
            $_wp_theme_features['post-thumbnails'][0][] = 'post';
        }

        return;
    }

    if (!in_array('tcode_event', $_wp_theme_features['post-thumbnails'][0])) {
        $_wp_theme_features['post-thumbnails'][0][] = 'tcode_event';
    }

    if (!in_array('tcode_artist', $_wp_theme_features['post-thumbnails'][0])) {
        $_wp_theme_features['post-thumbnails'][0][] = 'tcode_artist';
    }
}, 11);

add_filter('enter_title_here', function($title) {
    $screen = get_current_screen();

    if ($screen->post_type === 'tcode_artist') {
        $title = 'Enter author\'s name';
    }

    return $title;
});

if( function_exists('add_term_ordering_support') )
    add_term_ordering_support ('location');
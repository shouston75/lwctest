<div class="sidebar-latest-posts">
<h3 class="recent-heading"><span>RECENT POSTS</span></h3>
<?php $the_query = new WP_Query( 'posts_per_page=4' ); ?>
<?php while ($the_query -> have_posts()) : $the_query -> the_post(); ?>
	<div class="recent-box">
        <a href="<?php the_permalink() ?>"><div class="img-container"><?php the_post_thumbnail( 'thumbnail'); ?></div></a>
        <h1 class="recent-title"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h1>
	</div>
<?php  endwhile; wp_reset_postdata(); ?>
</div>
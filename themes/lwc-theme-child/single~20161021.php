<?php get_header(); ?>

    <div id="lwc-primary">
        <div class="lwc-ssc-left">
            <?php if ( is_active_sidebar( 'lwc_blog_left' ) ) : ?>
                <div id="lwc-ad-blog-left">
                    <script type="text/javascript">
                        if ( width < 1000 )
                        {
                            var elem = document.getElementById( 'lwc-ad-blog-left' );
                            elem.parentNode.removeChild(elem);
                        }
                    </script>
                    <?php dynamic_sidebar( 'lwc_blog_left' ); ?>
                </div>
            <?php endif; ?>
        </div>
        <div class="lwc-ssc-right">
            <?php echo do_shortcode( '[social-shares-count-desktop]' ); ?>
			<?php /*?><?php get_template_part( 'dsk-sky-ad-a' ); ?><?php */?>
        </div>
    </div>
    <div id="lwc-content" itemscope itemtype="https://schema.org/BlogPosting">
        <input type="hidden" class="ajax-img-src" value="<?php echo get_template_directory_uri(); ?>/images/ajax-loader.gif" />
        <div class="lwc-content-wrap">
            <?php
            $post_url = '';
            while ( have_posts() ) : the_post(); ?>
                <?php $post_url = get_the_permalink(); ?>
                <?php
                $pid = get_the_ID();
                $nextid = 0;
                if ( $pid > 0 ) {
                    global $wpdb;
                    $date = $wpdb->get_var( "SELECT `post_date` FROM $wpdb->posts WHERE `ID` = '$pid'");
                    $nextid = $wpdb->get_var( "SELECT p.ID FROM $wpdb->posts p WHERE p.post_date < '$date' AND p.post_type='post' AND p.post_status = 'publish' ORDER BY p.post_date DESC LIMIT 1");
                }
                $short_url = str_replace( 'http://louderwithcrowder.com', '', $post_url );
                ?>
                <input type="hidden" class="history-url" value="<?php echo $short_url; ?>" />
                <input type="hidden" class="load-next" value="<?php if ( $nextid > 0 ) { echo $nextid; }  ?>" />
                <input type="hidden" class="load-next-url" value="<?php if ( ! empty( $nextid ) ) { echo get_permalink( $nextid ); }  ?>" />
                <input type="hidden" class="load-next-title" value="<?php if ( ! empty( $nextid ) ) { echo get_the_title( $nextid ); }  ?>" />
                <input type="hidden" class="current-id" value="<?php echo $pid; ?>" />

                <h1 class="lwc-title" itemprop="headline"><?php echo get_the_title(); ?></h1>
                <div class="post-content">
                    <div class="lwc-post-info">
                        <span class="lwc-author" itemprop="author"><?php echo get_the_author(); ?></span>
                        <span class="lwc-date" itemprop="dataPublished"><?php echo get_the_date( 'l F j Y' ); ?></span>
                    </div>
                </div>
                
                <article class="type-wrap" itemprop="articleBody">
				<?php /*?><?php the_content(); ?><?php */?>
                
                <?php echo do_shortcode('[easy-social-share buttons="facebook,twitter,mail,more,google,pinterest,linkedin,reddit,digg,print" morebutton="1" morebutton_icon="plus" counters=0 counter_pos="hidden" total_counter_pos="leftbig" style="button" nospace="no" facebook_text="Share" twitter_text="Tweet" google_text="Plus" pinterest_text="Pin" linkedin_text="Link" digg_text="Digg" print_text="" mail_text="" reddit_text="Link" template="metro-retina"]'); ?>
                
				<?php
                    $content = apply_filters('the_content', get_the_content());
                    $content = explode("</p>", $content);
                    for ($i = 0; $i < count($content); $i++) { ?>

					<?php if ($i ==  2)  { get_template_part( 'mob-box-ad-a' ); } ?> 
                    <?php if ($i ==  6)  { get_template_part( 'ad-contextual-g' ); } ?> 
                    	<?php if ($i ==  6)  { get_template_part( 'dsk-box-ad-c' ); } ?> 
                    <?php if ($i ==  10)  { get_template_part( 'mob-box-ad-b' ); } ?> 
                    <?php /*?><?php if ($i == 12)  { get_template_part( 'mob-box-ad-c' ); } ?><?php */?>
                    	<?php if ($i == 12)  { get_template_part( 'dsk-box-ad-d' ); } ?> 
                    <?php /*?><?php if ($i == 19)  { get_template_part( 'mob-box-ad-d' ); } ?><?php */?>
                
				<?php echo $content[$i] . "</p>"; } ?>
                                    
                </article>
                
                <?php echo do_shortcode('[easy-social-share buttons="facebook,twitter,mail,more,google,pinterest,linkedin,reddit,digg,print" morebutton="1" morebutton_icon="plus" counters=0 counter_pos="hidden" total_counter_pos="leftbig" style="button" nospace="no" facebook_text="Share" twitter_text="Tweet" google_text="Plus" pinterest_text="Pin" linkedin_text="Link" digg_text="Digg" print_text="" mail_text="" reddit_text="Link" template="metro-retina"]'); ?>
                                
                <?php get_template_part( 'ad-contextual-a' ); ?>
                
                <?php echo do_shortcode('[wpdevart_facebook_comment curent_url="' . $post_url . '" order_type="social" title_text="Comments" title_text_color="#000000" title_text_font_size="22" title_text_font_famely="Times New Roman" title_text_position="left" width="100%" bg_color="#CCCCCC" animation_effect="none" count_of_comments="5" ]'); ?>
                <?php
					if ( comments_open() || get_comments_number() ) {
						comments_template();
					}
				endwhile;
				?>
        </div>
    </div>
    <div id="lwc-secondary">
        <div class="lwc-subscribe-email">
            <div>
                <i class="fa fa-envelope"></i>
                <span>subscribe to the email</span>
            </div>
            <form method="post" action="https://inboxfirst.maximtech.com/v1/" accept-charset="UTF-8">
                <input type="hidden" value="F8GR4NJKY94T" name="ApiKey">
                <input type="hidden" value="175" name="FormID">
                <input type="hidden" value="http://louderwithcrowder.com/welcome/" name="PassUrl">
                <input type="hidden" value="http://louderwithcrowder.com/failed-validation/" name="FailUrl">
                <input type="text" size="30" name="pending_subscriber[email]" class="subscribe-input">
                <button type="submit" class="subscribe-button">Subscribe</button>
            </form>
        </div>
        <div class="lwc-subscribe-podcast">
            <div>
                <a href="https://soundcloud.com/louderwithcrowder" title="subscribe to podcast" target="_blank" style="color: #ffffff; text-decoration: none;">
                    <i class="fa fa-microphone"></i>
                    <span>subscribe to the podcast</span>
                </a>
            </div>
        </div>
        <div class="lwc-send-tip">
            <div>
                <a href="mailto:teamcrowder@louderwithcrowder.com?subject=Suggestion%20for%20the%20Crowder%20Show" title="Send a News Article" target="_blank" style="color: #ffffff; text-decoration: none;">
                    <i class="fa fa-paper-plane"></i>
                    <span>Send a News Article/Scoop</span>
                </a>
            </div>
        </div>
        
        <?php get_template_part( 'dsk-box-ad-a' ); ?>
        <?php get_template_part( 'twitter-timeline' ); ?>
        <?php /*?><?php get_template_part( 'ad-contextual-b' ); ?><?php */?>
        <?php get_template_part( 'dsk-box-ad-b' ); ?>
        
        <?php if ( is_active_sidebar( 'lwc_blog_right' ) ) : ?>
            <div id="lwc-ad-blog-right">
                <script type="text/javascript">
                    if ( width < 1000 )
                    {
                        var elem = document.getElementById( 'lwc-ad-blog-right' );
                        elem.parentNode.removeChild(elem);
                    }
                </script>
                <?php /*?><?php dynamic_sidebar( 'lwc_blog_right' ); ?><?php */?>
            </div>
        <?php endif; ?>
    </div>

<?php get_footer(); ?>
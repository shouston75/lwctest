jQuery(document).ready(function($) {
    $('.ssc-desktop').addClass('ssc-desktop-0');
    $('.load-more').remove();
    var numLoaded = 0;
    loadPosts().then(function () {
        $('.load-more').on('click', loadMoreBtnClick);
    });

    function loadMoreBtnClick () {
        numLoaded++;
        loadPosts().then(function() {
            $('.load-more').remove();
            loadPosts().then(function() {
                if (numLoaded >= 10) {
                    $('.load-more').remove();
                } else {
                    $('.load-more').on('click', loadMoreBtnClick);
                }
            });
        });
    }

    function loadPosts(numPosts) {
        if( $('.load-next').length > 0 && $('.load-next-url').length > 0 && $('.load-next-title').length > 0 ) {
            var pid = $('.load-next').val();
            var purl = $('.load-next-url').val();
            var title = $('.load-next-title').val();
            if( '' != pid && '' != purl && '' != title ) {
                return getNextPost( pid, purl, title );
            }
        }
    }

    function getNextPost( pid, purl, title ){
        if( ! $('.loader-img').length > 0 ) {
            var ajaximagesrc = $('.ajax-img-src').val();
            $('#lwc-content').append('<p style="text-align: center;" class="loader-img"><img src="' + ajaximagesrc + '"></p>');
            return $.get(purl, function (res) {
                $('.loader-img').remove();
                $('.load-next').remove();
                $('.load-next-url').remove();
                $('.load-next-title').remove();
                var html = $(res).find('.lwc-content-wrap').clone();
                var ssc = $(res).find('.ssc-desktop').addClass('ssc-desktop-' + pid).css('display', 'none').clone();
                html.find('#lwc-ad-single-post-bottom').remove();
                html.find('.adblade-dyna').remove();
                html.find('#wpdevar_comment_1').remove();
                html.find('#comments').remove();
                html.find('#respond').remove();
                html.find('script').remove();
                lazy_load_init();
                $('#lwc-content').append(html);
                $('.lwc-ssc-right').append(ssc);
            });
        } else {
            return null;
        }
    }

    function lazy_load_init() {
        $( 'img[data-lazy-src]' ).bind( 'scrollin', { distance: 200 }, function() {
            lazy_load_image( this );
        });

        // We need to force load gallery images in Jetpack Carousel and give up lazy-loading otherwise images don't show up correctly
        $( '[data-carousel-extra]' ).each( function() {
            $( this ).find( 'img[data-lazy-src]' ).each( function() {
                lazy_load_image( this );
            } );
        } );
    }

    function lazy_load_image( img ) {
        var $img = jQuery( img ),
            src = $img.attr( 'data-lazy-src' );

        if ( ! src || 'undefined' === typeof( src ) )
            return;

        $img.unbind( 'scrollin' ) // remove event binding
            .hide()
            .removeAttr( 'data-lazy-src' )
            .attr( 'data-lazy-loaded', 'true' );

        img.src = src;
        $img.fadeIn();
    }

    var scrolling = false;

    $(window).scroll(function() {
        scrolling = true;
    });

    setInterval(function() {
        if ( scrolling ) {
            scrolling = false;
            var historyTitle = '';
            var historyUrl = '';
            var curr_id = '';
            $(".lwc-content-wrap:in-viewport").each(function() {
                historyTitle = $(this).find('h1.lwc-title').text();
                historyUrl = $(this).find('.history-url').val();
                curr_id = $(this).find('.current-id').val();
            });
            if( '' != historyTitle && '' != historyUrl && window.location.pathname != historyUrl && '' != curr_id ) {
                window.history.pushState(null, null, historyUrl);
                document.title = historyTitle;
                $('.ssc-desktop').each(function() {
                    if( $(this).parent().hasClass('is-sticky') ) {
                        $(this).unstick();
                    }
                    $(this).css('display', 'none');
                });
                if ( $('.ssc-desktop-' + curr_id).length > 0 ) {
                    $('.ssc-desktop-' + curr_id).sticky({topSpacing:40});
                    $('.ssc-desktop-' + curr_id).css('display', 'inherit');
                } else {
                    $('.ssc-desktop-0').sticky({topSpacing:40});
                    $('.ssc-desktop-0').css('display', 'inherit');
                }

                ga('send', { hitType: 'pageview', page: historyUrl });
                try {
                  twoOhSix.insertAds();
                  twoOhSix.insertContextualAds();
                  twoOhSix.insertNativeAds();
                } catch(e) {}
            }


        }
    }, 250);

    function sticky_relocate() {
        var window_top = $(window).scrollTop();
        var div_top = $('#sticky-anchor').scrollTop();
        if (window_top > div_top) {
            $('#sticky-social').removeClass('stick-first');
            $('#sticky-social').addClass('stick-now');
        } else {
            $('#sticky-social').removeClass('stick-now');
            $('#sticky-social').addClass('stick-first');
            $('#sticky-anchor').height(0);
        }
    }

    $(function() {
        $(window).scroll(sticky_relocate);
        sticky_relocate();
    });

});
jQuery(document).ready(function($)

{

    $( '.lwc-search' ).click( function( e ) {

        $( '.lwc-nav-right' ).html( '<form role="search" class="search-form" method="get" action="/"><input type="text" value="" name="s" class="s"><button type="submit"><i class="fa fa-search"></i></button></form>' );

    })



    $( '.lwc-nav' ).sticky({topSpacing:0});

    $( '.lwc-nav' ).on( 'sticky-start', function() { $( this ).addClass( 'lwc-sticky' ); });

    $( '.lwc-nav' ).on( 'sticky-end', function() { $( this ).removeClass( 'lwc-sticky' ); });



    $( '.sticky-widget' ).sticky({topSpacing:50});



    $( 'img.lazy' ).show();

    setTimeout(function(){

        $( 'img.lazy:in-viewport' ).lazyload({

            event: 'lazyload',

        }).trigger( 'lazyload' ).removeClass( 'lazy' );

    }, 10);



    $( window ).scroll(function() {

        $( 'img.lazy:in-viewport' ).lazyload({

            event: 'lazyload',

        }).trigger( 'lazyload' ).removeClass( 'lazy' );

    });



    $(window).resize(function()

    {

        $( 'img.lazy' ).show();

        $( '.sticky-widget' ).unstick();

        $( '.ssc-desktop' ).unstick();

        $( '.sticky-widget' ).sticky({topSpacing:50});

        $( '.ssc-desktop' ).sticky({topSpacing:40});

    });



});
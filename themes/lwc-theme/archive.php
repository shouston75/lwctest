<?php get_header(); ?>
<?php
$category_id = get_queried_object_id();
$cat = get_category( $category_id );
$total = ceil( $cat->category_count / 10 );
?>
    <script type="text/javascript">
        jQuery(document).ready(function($)
        {
            var page = 1;
            var total = <?php echo $total; ?>;
            $( window ).scroll(function(){
                if ( $( window ).scrollTop() == $( document ).height() - $( window ).height() ) {
                    if ( page > total ) {
                        return false;
                    }
                    else {
                        getPosts( page );
                    }
                    page++;
                }
            });

            function getPosts( page ){
                $( '.inifinite-load' ).show( 'fast' );
                $.ajax({
                    url: "<?php echo site_url(); ?>/wp-admin/admin-ajax.php",
                    dataType: 'json',
                    type: 'POST',
                    data: "action=infinite_load&category=<?php echo $category_id; ?>&page=" + page,
                    success: function( data ){
                        $( '.inifinite-load' ).hide( '1000' );
                        $( '.lwc-featured' ).append( data['featured'] );
                    }
                });
                return false;
            }
        });
    </script>
    <div id="lwc-primary">
        <div class="lwc-primary-wrap">
            <?php if ( is_active_sidebar( 'lwc_blog_left' ) ) : ?>
                <div id="lwc-ad-blog-left">
                    <script type="text/javascript">
                        if ( width < 1000 )
                        {
                            var elem = document.getElementById( 'lwc-ad-blog-left' );
                            elem.parentNode.removeChild(elem);
                        }
                    </script>
                    <?php dynamic_sidebar( 'lwc_blog_left' ); ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
    <div id="lwc-content">
        <?php
        $args = array(
            'numberposts' => 10,
            'offset' => 0,
            'category' => $category_id,
            'orderby' => 'post_date',
            'order' => 'DESC',
            'post_type' => 'post',
            'post_status' => 'publish'
        );
        $category_posts = wp_get_recent_posts( $args, ARRAY_A );
        ?>
        <?php if ( count( $category_posts ) > 0 ) : ?>
            <div class="lwc-featured">
                <?php foreach ( $category_posts as $category ) : ?>
                    <div class="featured-box">
                        <?php
                            $thumb_id = 0;
                            if ( has_post_thumbnail( $category['ID'] ) )
                            {
                                $thumb_id = get_post_thumbnail_id( $category['ID'] );
                                $image = wp_get_attachment_image_src( get_post_thumbnail_id( $category['ID'] ), 'blog-large' );
                            }
                            if ( empty( $image[0] ) )
                            {
                                $image[0] = get_template_directory_uri() . '/images/thumbnail-660x272.jpg';
                            }
                            $link = get_permalink( $category['ID'] );
                        ?>
                        <a href="<?php echo $link; ?>">
                            <div class="img-container">
                                <img alt="<?php echo lwc_image_alt( $thumb_id ); ?>" class="lazy attachment-blog-large wp-post-image" data-original="<?php echo $image[0]; ?>" style="display: none;">
                            </div>
                            <noscript><img alt="<?php echo lwc_image_alt( $thumb_id ); ?>" class="attachment-blog-large wp-post-image" src="<?php echo $image[0]; ?>"></noscript>
                        </a>
                        <h2 class="featured-title">
                            <a href="<?php echo $link; ?>"><?php echo $category['post_title']; ?></a>
                        </h2>
                        <p class="lwc-excerpt"><?php echo lwc_excerpt( $category['ID'], $category['post_content'] ); ?></p>
                    </div>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
    </div>
    <div id="lwc-secondary">
        <div class="lwc-subscribe-email">
            <div>
                <i class="fa fa-envelope"></i>
                <span>subscribe to the email</span>
            </div>
            <form method="post" action="https://inboxfirst.maximtech.com/v1/" accept-charset="UTF-8">
                <input type="hidden" value="F8GR4NJKY94T" name="ApiKey">
                <input type="hidden" value="175" name="FormID">
                <input type="hidden" value="http://louderwithcrowder.com/welcome/" name="PassUrl">
                <input type="hidden" value="http://louderwithcrowder.com/failed-validation/" name="FailUrl">
                <input type="text" size="30" name="pending_subscriber[email]" class="subscribe-input">
                <button type="submit" class="subscribe-button">Subscribe</button>
            </form>
        </div>
        <div class="lwc-subscribe-podcast">
            <div>
                <a href="https://soundcloud.com/louderwithcrowder" title="subscribe to podcast" target="_blank" style="color: #ffffff; text-decoration: none;">
                    <i class="fa fa-microphone"></i>
                    <span>subscribe to the podcast</span>
                </a>
            </div>
        </div>
        <?php if ( is_active_sidebar( 'lwc_blog_right' ) ) : ?>
            <div id="lwc-ad-blog-right">
                <script type="text/javascript">
                    if ( width < 1000 )
                    {
                        var elem = document.getElementById( 'lwc-ad-blog-right' );
                        elem.parentNode.removeChild(elem);
                    }
                </script>
                <?php dynamic_sidebar( 'lwc_blog_right' ); ?>
            </div>
        <?php endif; ?>
    </div>

<?php get_footer(); ?>
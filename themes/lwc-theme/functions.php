<?php

function lwc_scripts() {
    wp_enqueue_style( 'main', get_stylesheet_directory_uri() . '/style.css', array(), '1.1.6' );
    wp_enqueue_style( 'lwc-typesettings', get_template_directory_uri() . '/css/typesettings.css' );
    wp_enqueue_style( 'lwc-fontawesome', get_template_directory_uri() . '/fonts/font-awesome/css/font-awesome.min.css' );
    wp_enqueue_style( 'lwc-alegreya', '//fonts.googleapis.com/css?family=Alegreya+Sans%3A400%2C400italic%2C700%2C700italic&#038;subset=latin&#038' );
    wp_enqueue_style( 'modalcss', get_template_directory_uri() . '/css/modal.css' );
    wp_enqueue_script( 'jquery', get_template_directory_uri() . '/js/jquery-1.11.3.min.js', array(), '1.0.0', true );
    wp_enqueue_script( 'modaljs', get_template_directory_uri() . '/js/modal.js', array('jquery'), '1.0.1', true );
    wp_enqueue_script( 'customjs', get_template_directory_uri() . '/js/custom.js', array('jquery','modaljs'), '1.0.4');
    wp_enqueue_script( 'lazyload', get_template_directory_uri() . '/js/jquery.lazyload.min.js', array(), '1.0.0', true );
    wp_enqueue_script( 'lazyviewport', get_template_directory_uri() . '/js/jquery.viewport.mini.js', array(), '1.0.0', true );
    wp_enqueue_script( 'cookiejs', get_template_directory_uri() . '/js/cookie.js', array(), '1.0.0');

    if( is_single() ) {
        wp_enqueue_script( 'load-post', get_template_directory_uri() . '/js/load-post.js', array( 'wpcom-lazy-load-images', 'jquery-sonar' ), '1.0.8', true );
    }
}

add_action( 'wp_enqueue_scripts', 'lwc_scripts' );

register_nav_menus(
    array(
        'top-menu' => 'Top Menu',
        'bottom-menu' => 'Bottom Menu'
    )
);

add_theme_support( 'post-thumbnails' );
add_theme_support( 'post-formats', array( 'aside', 'gallery', 'link', 'image', 'quote', 'video', 'audio', 'chat' ) );

if ( function_exists( 'add_image_size' ) )
{
    add_image_size( 'blog-medium', 320, 202, true );
    add_image_size( 'blog-large', 669, 272, true );
}

function lwc_excerpt( $id, $content )
{
    $content = strip_shortcodes( $content );
    $content = apply_filters( 'the_content', $content );
    $content = str_replace( ']]>', ']]&gt;', $content );
    $length = apply_filters( 'excerpt_length', 35 );
    $more = '<a href="'. get_permalink( $id ) . '"> [...]</a>';
    $content = wp_trim_words( $content, $length, $more );
    return $content;
}

function lwc_widgets_init()
{

    register_sidebar( array(
        'name'          => 'Home right sidebar',
        'id'            => 'lwc_home_right',
        'before_widget' => '<div>',
        'after_widget'  => '</div>',
    ) );

    register_sidebar( array(
        'name'          => 'Desktop Ad Leaderboard',
        'id'            => 'lwc_desktop_ad_leaderboard',
        'before_widget' => '<div class="lwc-desktop-ad-leaderboard">',
        'after_widget'  => '</div>',
    ) );

    register_sidebar( array(
        'name'          => 'Desktop Ad Single Post Bottom',
        'id'            => 'lwc_desktop_ad_single_post_bottom',
        'before_widget' => '<div>',
        'after_widget'  => '</div>',
    ) );

    register_sidebar( array(
        'name'          => 'Mobile Ad Single Post Bottom',
        'id'            => 'lwc_mobile_ad_single_post_bottom',
        'before_widget' => '<div>',
        'after_widget'  => '</div>',
    ) );

    register_sidebar( array(
        'name'          => 'Mobile Home Recent 2nd',
        'id'            => 'lwc_mobile_home_recent_2nd',
        'before_widget' => '<div>',
        'after_widget'  => '</div>',
    ) );

    register_sidebar( array(
        'name'          => 'Mobile Home Recent 5th',
        'id'            => 'lwc_mobile_home_recent_5th',
        'before_widget' => '<div>',
        'after_widget'  => '</div>',
    ) );

    register_sidebar( array(
        'name'          => 'Blog left sidebar',
        'id'            => 'lwc_blog_left',
        'before_widget' => '<div>',
        'after_widget'  => '</div>',
    ) );

    register_sidebar( array(
        'name'          => 'Blog right sidebar',
        'id'            => 'lwc_blog_right',
        'before_widget' => '<div>',
        'after_widget'  => '</div>',
    ) );
}

add_action( 'widgets_init', 'lwc_widgets_init' );


function lwc_image_alt( $id )
{
    $alt = get_post_meta( $id, '_wp_attachment_image_alt', true );
    if ( empty( $alt ) )
    {
        $alt = get_post_field( 'post_excerpt', $id );
    }
    if ( empty( $alt ) )
    {
        $alt = get_post_field( 'post_title', $id );
    }
    return $alt;
}


function wp_infiniteload()
{
    $page = ! empty( $_POST['page'] ) ? intval( $_POST['page'] ) : 0;
    $data = array();
    $data['recent'] = '';
    $data['featured'] = '';
    if ( ! empty( $page ) )
    {
        if ( empty( $_POST['category'] ) )
        {
            $args = array(
                'numberposts' => 8,
                'offset' => $page * 8,
                'orderby' => 'post_date',
                'order' => 'DESC',
                'post_type' => 'post',
                'post_status' => 'publish'
            );
            $recent_posts = wp_get_recent_posts( $args, ARRAY_A );

            if ( count( $recent_posts ) > 0 )
            {
                foreach ( $recent_posts as $recent )
                {
                    $recentfeatured = '';
                    if ( has_category( '2181', $recent['ID'] ) )
                    {
                        $recentfeatured = 'recent-featured';
                    }

                    $data['recent'] .= '<div class="recent-box ' . $recentfeatured . '">';

                    $thumb_id = 0;
                    if ( has_post_thumbnail( $recent['ID'] ) )
                    {
                        $thumb_id = get_post_thumbnail_id( $recent['ID'] );
                        $image = wp_get_attachment_image_src( $thumb_id, 'blog-medium' );
                    }
                    if ( empty( $image[0] ) )
                    {
                        $image[0] = get_template_directory_uri() . '/images/thumbnail-320x202.jpg';
                    }
                    $link = get_permalink( $recent['ID'] );

                    if ( class_exists( 'LazyLoad_Images' ) ) {
                        $data['recent'] .= '<a href="' . $link . '"><div class="img-container"><img alt="' . lwc_image_alt( $thumb_id ) . '" class="lazy attachment-blog-medium wp-post-image" data-original="' . $image[0] . '" style="display: none;"></div></a>';
                        $data['recent'] .= '<noscript><img alt="' . lwc_image_alt( $thumb_id ) . '" class="attachment-blog-medium wp-post-image" src="' . $image[0] . '"></noscript>';
                    } else {
                        $data['recent'] .= '<a href="' . $link . '"><div class="img-container"><img alt="' . lwc_image_alt( $thumb_id ) . '" class="attachment-blog-medium wp-post-image" src="' . $image[0] . '"></div></a>';
                    }
                    $data['recent'] .= '<h1 class="recent-title"><a href="' . $link . '">' . $recent['post_title'] .'</a></h1></a>';
                    $data['recent'] .= '</div>';
                }
            }
        }

        $category_id = ! empty( $_POST['category'] ) ? intval( $_POST['category'] ) : 2181;

        $args = array(
            'numberposts' => 6,
            'offset' => $page * 6,
            'category' => $category_id,
            'orderby' => 'post_date',
            'order' => 'DESC',
            'post_type' => 'post',
            'post_status' => 'publish'
        );
        $featured_posts = wp_get_recent_posts( $args, ARRAY_A );

        if ( count( $featured_posts ) > 0 )
        {
            foreach ( $featured_posts as $featured )
            {
                $data['featured'] .= '<div class="featured-box">';
                $thumb_id = 0;
                if ( has_post_thumbnail( $featured['ID'] ) )
                {
                    $thumb_id = get_post_thumbnail_id( $featured['ID'] );
                    $image = wp_get_attachment_image_src( $thumb_id, 'blog-large' );
                }
                if ( empty( $image[0] ) )
                {
                    $image[0] = get_template_directory_uri() . '/images/thumbnail-660x272.jpg';
                }
                $link = get_permalink( $featured['ID'] );
                $excerpt = lwc_excerpt( $featured['ID'], $featured['post_content'] );
                if ( class_exists( 'LazyLoad_Images' ) ) {
                    $data['featured'] .= '<a href="' . $link . '"><div class="img-container"><img alt="' . lwc_image_alt( $thumb_id ) . '" class="lazy attachment-blog-large wp-post-image" data-original="' . $image[0] . '" style="display: none;"></div></a>';
                    $data['featured'] .= '<noscript><img alt="' . lwc_image_alt( $thumb_id ) . '" class="attachment-blog-medium wp-post-image" src="' . $image[0] . '"></noscript>';
                } else {
                    $data['featured'] .= '<a href="' . $link . '"><div class="img-container"><img alt="' . lwc_image_alt( $thumb_id ) . '" class="attachment-blog-large wp-post-image" src="' . $image[0] . '"></div></a>';
                }
                $data['featured'] .= '<h1 class="featured-title"><a href="' . $link . '">' . $featured['post_title'] .'</a></h1></a>';
                $data['featured'] .= '<p class="lwc-excerpt">' . $excerpt . '</p>';
                $data['featured'] .= '</div>';
            }
        }
        echo json_encode( $data );
        die();
    }
}

add_action('wp_ajax_infinite_load', 'wp_infiniteload');
add_action('wp_ajax_nopriv_infinite_load', 'wp_infiniteload');


function modify_comment_form_fields($fields){
    $commenter = wp_get_current_commenter();
    $req	   = get_option( 'require_name_email' );

    $fields['author'] = '<div id="comment-input"><input type="text" name="author" id="author" value="'. esc_attr( $commenter['comment_author'] ) .'" placeholder="Name (required)" size="22" tabindex="1" '. ( $req ? 'aria-required="true"' : '' ).' class="input-name" />';

    $fields['email'] = '<input type="text" name="email" id="email" value="'. esc_attr( $commenter['comment_author_email'] ) .'" placeholder="Email (required)" size="22" tabindex="2" '. ( $req ? 'aria-required="true"' : '' ).' class="input-email"  />';

    $fields['url'] = '<input type="text" name="url" id="url" value="'. esc_attr( $commenter['comment_author_url'] ) .'" placeholder="Website" size="22" tabindex="3" class="input-website" /></div>';

    return $fields;
}
add_filter('comment_form_default_fields','modify_comment_form_fields');

function lwc_comment( $comment, $args, $depth ) { ?>
    <?php $add_below = ''; ?>
    <li <?php comment_class(); ?> id="comment-<?php comment_ID(); ?>">
    <div class="the-comment">
        <div class="avatar"><?php echo get_avatar( $comment, 54 ); ?></div>
        <div class="comment-box">
            <div class="comment-author meta">
                <strong><?php echo get_comment_author_link(); ?></strong>
                <?php printf( '%1$s at %2$s', get_comment_date(),  get_comment_time() ); ?><?php edit_comment_link( ' - Edit','  ','' ); ?><?php comment_reply_link( array_merge( $args, array( 'reply_text' => ' - Reply', 'add_below' => 'comment', 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
            </div>
            <div class="comment-text">
                <?php if ( $comment->comment_approved == '0' ) : ?>
                    <em>Your comment is awaiting moderation.</em>
                    <br />
                <?php endif; ?>
                <?php comment_text() ?>
            </div>
        </div>
    </div>
    <?php
}

function lwc_fluid_videos( $content )
{
    $pattern = '~<iframe.*</iframe>|<embed.*</embed>|<object.*</object>~';
    preg_match_all( $pattern, $content, $matches);
    foreach ( $matches[0] as $match )
    {
        if( false !== strpos( strtolower( $match ), 'youtube.com' ) )
        {
            $wrappedframe = '<div class="fluid-width-video-wrapper">' . $match . '</div>';
            $content = str_replace( $match, $wrappedframe, $content );
        }
    }
    return $content;
}
add_filter( 'the_content', 'lwc_fluid_videos' );
